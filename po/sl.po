# Slovenian translation for gnubik.
# Copyright (C) 2011 John Darrington.
# This file is distributed under the same license as the gnubik package.
# Klemen Košir <klemen.kosir@gmx.com>, 2011.
#
msgid ""
msgstr ""
"Project-Id-Version: gnubik 2.4-pre1\n"
"Report-Msgid-Bugs-To: bug-gnubik@gnu.org\n"
"POT-Creation-Date: 2011-03-09 11:00+0100\n"
"PO-Revision-Date: 2011-04-09 09:50+0100\n"
"Last-Translator: Klemen Košir <klemen.kosir@gmx.com>\n"
"Language-Team: Slovenian <translation-team-sl@lists.sourceforge.net>\n"
"Language: sl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n"
"%100==4 ? 3 : 0);\n"

#: src/colour-dialog.c:184
msgid "Image Selector"
msgstr "Izbirnik slik"

#. TRANSLATORS: "All  Images" implies nothing but images!
#: src/colour-dialog.c:210
msgid "All Images"
msgstr "Vse slike"

#: src/colour-dialog.c:214
msgid "All Files"
msgstr "Vse datoteke"

#: src/colour-dialog.c:297
msgid "Colour selector"
msgstr "Izbirnik barv"

#: src/colour-dialog.c:314
msgid "Image"
msgstr "Slika"

#: src/colour-dialog.c:315
msgid "Se_lect"
msgstr "Iz_beri"

#: src/colour-dialog.c:323
msgid "Select an image file"
msgstr "Izberite slikovno datoteko"

#: src/colour-dialog.c:325
msgid "_Tiled"
msgstr "_Razpostavljeno"

#: src/colour-dialog.c:327
msgid "Place a copy of the image on each block"
msgstr "Postavi kopijo slike na vsako polje"

#: src/colour-dialog.c:332
msgid "_Mosaic"
msgstr "_Mozaik"

#: src/colour-dialog.c:336
msgid "Place a copy of the image across the entire face of the cube"
msgstr "Postavi kopijo slike čez celotno ploskev kocke"

#: src/colour-dialog.c:342
msgid "_Plain"
msgstr "_Običajno"

#: src/colour-dialog.c:345
msgid "Remove image from the cube face"
msgstr "Odstrani sliko s ploskve kocke"

#: src/dialogs.c:106
msgid "Size of cube:"
msgstr "Velikost kocke:"

#: src/dialogs.c:111
msgid "Re_gular cube"
msgstr "_Navadna kocka"

#: src/dialogs.c:117
msgid "Sets the number of blocks in each side"
msgstr "Nastavi število kockic na vsaki strani"

#: src/dialogs.c:121
msgid "Allow only cubes with all sides equal size"
msgstr "Dovoli samo kocke, ki imajo vse stranice enako velike"

#: src/dialogs.c:164
msgid "A 3 dimensional magic cube puzzle"
msgstr "Sestavljanka s čarobno tridimenzionalno kocko"

#. TRANSLATORS: Do not translate this string. Instead, use it to list
#. the people who have helped with translation to your language.
#: src/dialogs.c:169
msgid "translator-credits"
msgstr "Klemen Košir <https://launchpad.net/~klemen.kosir>"

#: src/dialogs.c:189
msgid "New Game"
msgstr "Nova igra"

#: src/dialogs.c:201
msgid "Dimensions"
msgstr "Mere"

#: src/dialogs.c:212
msgid "Initial position"
msgstr "Začetni položaj"

#: src/dialogs.c:216
msgid "_Random"
msgstr "_Naključno"

#: src/dialogs.c:220
msgid "_Solved"
msgstr "_Rešena kocka"

#: src/menus.c:94
msgid "Rear View"
msgstr "Pogled z zadnje strani"

#: src/menus.c:102
msgid "Bottom View"
msgstr "Pogled s spodnje strani"

#: src/menus.c:109
msgid "Top View"
msgstr "Pogled z zgornje strani"

#: src/menus.c:117
msgid "Left View"
msgstr "Pogled z leve strani"

#: src/menus.c:124
msgid "Right View"
msgstr "Pogled z desne strani"

#: src/menus.c:138
#, c-format
msgid "A mark is now set at position %d."
msgstr "Oznaka je nastavljena na položaju %d."

#: src/menus.c:152
#, c-format
msgid "Moves: %d / %d"
msgstr "Premiki: %d / %d"

#: src/menus.c:173
#, c-format
msgid "Cube solved in %d move"
msgid_plural "Cube solved in %d moves"
msgstr[0] "Kocka je bila rešena v %d premikih"
msgstr[1] "Kocka je bila rešena v %d premiku"
msgstr[2] "Kocka je bila rešena v %d premikih"
msgstr[3] "Kocka je bila rešena v %d premikih"

#: src/menus.c:183
msgid ""
"Cube is NOT solved! The colours are correct,  but have incorrect orientation"
msgstr "Kocka NI rešena! Barve so pravilne, vendar imajo napačno usmerjenost"

#: src/menus.c:201
#, c-format
msgid "Animation rate set to %d frame per turn."
msgid_plural "Animation rate set to %d frames per turn."
msgstr[0] "Hitrost animacije je nastavljena na %d sličic na obrat."
msgstr[1] "Hitrost animacije je nastavljena na %d sličico na obrat."
msgstr[2] "Hitrost animacije je nastavljena na %d sličici na obrat."
msgstr[3] "Hitrost animacije je nastavljena na %d sličice na obrat."

#: src/menus.c:228
msgid "_Game"
msgstr "_Igra"

#: src/menus.c:229
msgid "_View"
msgstr "_Pogled"

#. TRANSLATORS: Lets the user see the cube from a different angle
#: src/menus.c:231
msgid "Add _View"
msgstr "Dodaj _pogled"

#: src/menus.c:232
msgid "Add an auxiliary view of the cube"
msgstr "Dodaj pomožen pogled kocke"

#: src/menus.c:235
msgid "_Help"
msgstr "Pomo_č"

#: src/menus.c:236
msgid "Sho_w/Hide"
msgstr "_Pokaži/Skrij"

#: src/menus.c:237
msgid "_Scripts"
msgstr "_Skripti"

#: src/menus.c:307
msgid "_Restart Game"
msgstr "_Začni znova"

#: src/menus.c:311
msgid "_New Game"
msgstr "_Nova igra"

#: src/menus.c:315
msgid "_Rear"
msgstr "_Zadaj"

#: src/menus.c:317
msgid "_Left"
msgstr "_Levo"

#: src/menus.c:319
msgid "Ri_ght"
msgstr "_Desno"

#: src/menus.c:321
msgid "_Top"
msgstr "_Zgoraj"

#: src/menus.c:323
msgid "_Bottom"
msgstr "_Spodaj"

#: src/menus.c:328
msgid "_Colours"
msgstr "_Barve"

#: src/menus.c:332
msgid "_Animation"
msgstr "_Animacija"

#: src/menus.c:336
msgid "_Faster"
msgstr "_Hitreje"

#: src/menus.c:340
msgid "_Slower"
msgstr "_Počasneje"

#: src/menus.c:455
msgid "Rewind"
msgstr "Previj"

#: src/menus.c:456
#, fuzzy
msgid "Go to the previous mark (or the beginning of the sequence of moves)"
msgstr "Vrni se na prejšnjo oznako (ali začetek) zaporedja premikov"

#: src/menus.c:462
msgid "Back"
msgstr "Nazaj"

#: src/menus.c:463
msgid "Make one step backwards"
msgstr "Premakni se za en korak nazaj"

#: src/menus.c:468
msgid "Stop"
msgstr "Zaustavi"

#: src/menus.c:469
msgid "Stop running the sequence of moves"
msgstr "Zaustavi predvajanje zaporedja premikov"

#: src/menus.c:475
msgid "Mark"
msgstr "Označi"

#: src/menus.c:476
msgid "Mark the current place in the sequence of moves"
msgstr "Označi trenutno mesto v zaporedju premikov"

#: src/menus.c:482
msgid "Forward"
msgstr "Naprej"

#: src/menus.c:483
msgid "Make one step forwards"
msgstr "Premakni se za en korak naprej"

#: src/menus.c:488
msgid "Play"
msgstr "Predvajaj"

#: src/menus.c:489
msgid "Run forward through the sequence of moves"
msgstr "Predvajaj zaporedje premikov"

#: src/menus.c:555
msgid "Gnubik error"
msgstr "Napaka Gnubik"

#: src/swatch.c:132
msgid ""
"A sample of the colour. You can click and select a new colour, or drag one "
"to this space."
msgstr ""
"Vzorec barve. Tukaj lahko kliknete in izberete novo barvo, ali pa jo v to "
"polje premaknete."

#: src/textures.c:81
msgid "Pixbuf has wrong number of channels"
msgstr "Medpomnilnik sličic ima napačno število kanalov"

#: src/textures.c:90
#, c-format
msgid "Pixbuf has unknown colorspace: %d"
msgstr "Pixbuf ima neznan barvni prostor: %d"

#: scripts/debug.scm:50
msgid "_Debug"
msgstr "_Razhroščevanje"

#: scripts/debug.scm:52
msgid "_Move"
msgstr "_Premakni"

#: scripts/debug.scm:54
msgid "_Dump state"
msgstr "_Odloži stanje"

#: scripts/rand.scm:38
msgid "_Randomize"
msgstr "_Naključno"

#: scripts/rand.scm:40 scripts/rand.scm:41 scripts/rand.scm:42
#: scripts/rand.scm:43 scripts/rand.scm:44 scripts/rand.scm:45
#: scripts/rand.scm:46 scripts/rand.scm:47
#, scheme-format
msgid "_~a"
msgstr ""

#: scripts/flubrd.scm:50
msgid "This script only works on 3×3×3 cubes."
msgstr "Ta skript deluje samo na kockah 3×3×3."

#: scripts/mellor-solve.scm:399
msgid "_Solvers"
msgstr "_Reševalniki"

#: scripts/mellor-solve.scm:400
msgid "_3×3"
msgstr "_3×3"

#: scripts/mellor-solve.scm:404
msgid "_Full cube"
msgstr "_Polna kocka"

#: scripts/mellor-solve.scm:407
msgid "Bottom _edge place"
msgstr "Položaj spodnjih _robov"

#: scripts/mellor-solve.scm:410
msgid "Bottom _corner orient"
msgstr "Usmerjenost spodnjih _kotov"

#: scripts/mellor-solve.scm:413
msgid "_Bottom corner place"
msgstr "Položaj _spodnjih kotov"

#: scripts/mellor-solve.scm:416
msgid "_Middle slice"
msgstr "_Srednja plast"

#: scripts/mellor-solve.scm:419
msgid "_Top slice"
msgstr "_Zgornja plast"

#: scripts/mellor-solve.scm:422
msgid "_Top edges"
msgstr "_Zgornji koti"

#~ msgid "_Animated"
#~ msgstr "_Animirano"

#~ msgid "_Fast"
#~ msgstr "_Hitro"

#~ msgid "Cannot create image from file %s: %s"
#~ msgstr "Slike iz datoteke %s ni mogoče ustvariti: %s"

#~ msgid "Pattern"
#~ msgstr "Vzorec"

#~ msgid "Click here to use a pattern on the cube surface"
#~ msgstr "Če želite na površini kocke uporabiti vzorec, kliknite sem"

#~ msgid "Controls the speed with which slices rotate"
#~ msgstr "Nadzoruje hitrost vrtenja plasti, vrstic in stolpcev"

#~ msgid "Preferences"
#~ msgstr "Možnosti"

#~ msgid "Lighting"
#~ msgstr "Osvetljevanje"

#~ msgid "Enable lighting"
#~ msgstr "Omogoči osvetljevanje"

#~ msgid "Makes the cube appear illuminated"
#~ msgstr "Osvetli kocko"

#~ msgid "Start cube with new settings?"
#~ msgstr "Ali želite začeti kocko z novimi nastavitvami?"

#~ msgid "_Play Toolbar"
#~ msgstr "Orodna vrstica"

#~ msgid "_Status Bar"
#~ msgstr "Vrstica _stanja"

#~ msgid "/_Game/sep"
#~ msgstr "/_Igra/sep"

#~ msgid "/_Game/_Quit"
#~ msgstr "/I_gra/_Končaj"

#~ msgid "/_Settings"
#~ msgstr "/Na_stavitve"

#~ msgid "/_Settings/_Preferences"
#~ msgstr "/Na_stavitve/_Možnosti"

#~ msgid "/_Settings/_Colours"
#~ msgstr "/Na_stavitve/_Barve"

#~ msgid "/_Help/_About"
#~ msgstr "/Pomo_č/_O programu"

#~ msgid "/Settings/Show\\/Hide"
#~ msgstr "/Nastavitve/Pokaži\\/Skrij"
